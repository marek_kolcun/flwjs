function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

let flw = new Flw('.example');
let app = new Elem('app', {
    fontsize: {
        current: 12,
        max: 20
    },
    emails: {
        list: []
    },
    test: 'Hello. Try to move the range, click on the button and click on the\
    list elements then.',

    _addCard: (ev, me) => {
        let ne = new Elem('app__email');
        ne.props = {
            myKey: ne.getKey(),
            color: getRandomColor(),
            _remove: (ev, me) => {
                let emails = app.getValue('emails.list').reduce((x, y) => {
                    if (y.getKey() != me.getKey())
                        x.push(y);
                    return x;
                }, []);

                app.setValue('emails.list', emails);
            }
        };
        
        let list = me.getValue('emails.list');
        list.push(ne);
        me.setValue('emails.list', list);
    }
});

flw.addElem(app, 'mojkluc');
flw.run();