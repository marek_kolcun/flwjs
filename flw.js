/**
 * Flw - wrap bindings faster
 * 
 * Flw is a simple wrapper to handle 2-way databinding.
 * 
 * @author Marek J. Kolcun
 * @version 0.1.0
 */

/**
 * Main Flw class.
 * 
 * This class generates an instance of main object that handles
 * all the other flw-based elements.
 * 
 * @public
 * @class
 */
class Flw {
    /**
     * A Flw constructor method.
     * 
     * @param {string} rootSelector - an argument for `document.querySelector()`
     * @param {Map} elems           - optional Map of pre-generated Elems
     */
    constructor(rootSelector, elems) {
        this.root = document.querySelector(rootSelector);
        this.elems = elems || new Map();
    }

    /**
     * Adds an Elem instance info list of handled objects.
     * 
     * @public
     * @param {Elem} elem  - an Elem class instance
     * @param {string} key - identifier
     */
    addElem(elem, key = null) {
        key = key || elem.getKey();
        elem.setKey(key);
        this.elems.set(key, elem);
    }

    /**
     * Removes an Elem instance from list of handled objects.
     * 
     * @public
     * @param {string} key - Elem identifier
     */
    removeElem(key) {
        if (!this.hasElem(key))
            return;
        
        this.elems.delete(key);
        document.querySelector('[data-k="' + key + '"').remove();
    }

    /**
     * Checks whether an Elem instance with given `key` is already handled.
     * 
     * @public
     * @param {string} key - Elem identifier
     * @return {bool}      - true if `key` is already registered
     */
    hasElem(key) {
        return this.elems.has(key);
    }

    /**
     * Main method that launches all the Flw functionality.
     * 
     * @public
     */
    run() {
        while (this.root.childNodes.length) {
            this.root.firstChild.remove();
        }
        
        this.elems.forEach((x, y) => {
            let r = x.renderer.render();
            if (x.isElement) {
                r.dataset.k = y;
            }
            
            this.root.appendChild(r);
        });
    }
}

/**
 * Elem is a basic building block of Flw functionality.
 * 
 * Every Elem object handles part of the application functionality, being it a
 * form, a login block, a shopping cart, a list of goods, etc.
 * 
 * @public
 * @class
 */
class Elem {
    /**
     * An Elem constructor method.
     *
     * @param {string} templateName - it may be either a name of the template,
     *                                or a Text node content
     * @param {object} props        - object carrying all the props given to Elem 
     */
    constructor(templateName, props = {}) {
        this.templateName = templateName;
        this.renderer = new Renderer(this);
        this.props = props;
        this.key = this.generateKey(this.templateName);
        this.isElement = this.renderer.template.children.length 
            && this.renderer.template.children[0] instanceof HTMLElement;
    }

    /**
     * Generates a random key with given prefix.
     * 
     * @private
     * @param {string} prefix - basically any string
     */
    generateKey(prefix) {
        return [
            prefix,
            (new Date()).getTime(),
            Math.floor(Math.random() * 1000)
        ].join('_');
    }

    /**
     * Sets the Elem's `key` identifier.
     * 
     * @private
     * @param {string} key - new Elem key
     */
    setKey(key) {
        this.key = key;
    }

    /**
     * Returns Elem's `key` identifier.
     * 
     * @public
     * @return {string} - Elem's key
     */
    getKey() {
        return this.key;
    }

    /**
     * Returns props value by given path.
     * 
     * @public
     * @param {string} path - a dot-separated path to the props attribute
     * @param {*} data      - used for recursive call
     * @return {mixed}      - returns value of the targeted props attribute
     */
    getValue(path, data = null) {
        path = path instanceof Array ? path : path.split('.');
        data = data || this.props;
        
        let part = path.shift();

        if (typeof data[part] === 'undefined') {
            return null;
        }

        if (path.length) {
            return this.getValue(path, data[part]);
        }
        else {
            return data[part];
        }
    }

    /**
     * Sets the value into Elem's props by given path.
     * 
     * @public
     * @param {*} path  - dot-separated path to the props attribute; if the path
     *                    doesn't exist, it will be created.
     * @param {*} value - value to be stored in the props
     * @param {*} data  - used for recursive call
     */
    setValue(path, value, data = null) {
        path = path instanceof Array ? path : path.split('.');
        data = data || this.props;

        let part = path.shift();
        
        if (path.length) {
            this.setValue(path, value, data[part] || {});
        }
        else {
            data[part] = value;
            this.renderer.update();
        }
    }
}

/**
 * A class that renders Elems into the DOM.
 * 
 * Main class that handles Elem internal template and transforms it into the
 * HTML element that is then passed into DOM.
 * 
 * @public
 * @class
 */
class Renderer {
    /**
     * An Elem constructor method.
     */
    constructor(elem) {
        this.elem = elem;
        this.template = this.findTemplate(elem.templateName);
    }

    /**
     * Tries to find a <template> defined in HTML by its name; if the template
     * is not found, a Text node with `templateName` as its content is created.
     * 
     * @private
     * @param {string} templateName - name of the <template> to be found
     * @return {HTMLElement|Text}   - a node to be returned
     */
    findTemplate(templateName) {
        let template = document.querySelector('[name="' + templateName + '"]');
        if (template !== null) {
            return template.content.cloneNode(true)
        }
        else {
            let t = new Text();
            t.textContent = templateName;
            return t;
        }
    }

    /**
     * Generates a DOM interpretation of an Elem instance.
     * 
     * @public
     * @param {*} node - used for recursive call
     */
    render(node) {
        node = node || this.template.cloneNode(true);
        
        if (node instanceof DocumentFragment) {
            for (let i in node.childNodes) {
                if (!node.childNodes.hasOwnProperty(i))
                    continue;
                node.childNodes
                    .item(i)
                    .replaceWith(this.render(node.childNodes[i]));
            }
        }
        else if (node instanceof HTMLElement) {
            // key
            if (typeof node.dataset.k !== 'undefined') {
                node.dataset.k = this.elem.getKey();
            }

            // events
            if (typeof node.dataset.e !== 'undefined') {
                this.findAndInvokeEvents(node, node.dataset.e);
                // delete(node.dataset.e);
            }
            
            // attributes
            for (let i in node.attributes) {
                if (!node.attributes.hasOwnProperty(i))
                    continue;
                
                node.attributes[i].value = this
                    .findAndReplaceBindings(node.attributes[i], true)
                    .textContent;
            }

            // autobind form element events
            this.invokeAutoEvents(node);

            // children
            for (let i in node.childNodes) {
                if (!node.childNodes.hasOwnProperty(i))
                    continue;
                node.childNodes[i].replaceWith(this.render(node.childNodes[i]));
            }
        }
        else if (node instanceof Node) {
            let newNode = this.findAndReplaceBindings(node);
            
            if (newNode instanceof DocumentFragment) {
                node.replaceWith(... newNode.childNodes);
            }
            else {
                node.replaceWith(newNode);
            }
        }

        return node instanceof DocumentFragment ? node.children[0] : node;
    }

    /**
     * Used when an existing DOM interpretation of Elem should be updated.
     * 
     * When none attribute is provided, default values from existing DOM and
     * Elem's template are generated; then both HTMLElements are traversed and
     * existing DOM node is updated based on the values of generated Node.
     * 
     * @public
     * @param {Node} temp - an existing Node in DOM
     * @param {Node} rend - a generated Node that serves as template
     */
    update(temp, rend) {
        temp = temp || document.querySelector('[data-k="' + this.elem.key + '"]');
        rend = rend || this.render();

        if (temp === null || temp instanceof Text && temp.textContent == '') {
            temp.replaceWith(rend);
        }
        else {
            if (temp instanceof HTMLElement) {
                // remove old attributes
                for (let i in temp.attributes) {
                    if (!temp.attributes.hasOwnProperty(i))
                        continue;
                    
                    let found = false;
                    for (let j in rend.attributes) {
                        if (!rend.attributes.hasOwnProperty(j))
                            continue;
                        
                        if (rend.attributes[j].name == temp.attributes[i].name) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        temp.removeAttribute(temp.attributes[i].name);
                    }
                }

                //update existing attributes
                for (let i in rend.attributes) {
                    if (!rend.attributes.hasOwnProperty(i)
                        || temp instanceof Text
                        || (typeof temp.attributes[i] !== 'undefined'
                            && temp.attributes[i].value == rend.attributes[i].value)
                    )
                        continue;
                    
                    temp.setAttribute(
                        rend.attributes[i].name,
                        rend.attributes[i].value
                    );
                }
            }

            // handle child nodes based on their amount
            if (
                !rend.childNodes.length
                && temp.textContent != rend.textContent
            ) {
                temp.replaceWith(rend);
            }
            else if (temp.childNodes.length <= rend.childNodes.length) {
                for (let i in rend.childNodes) {
                    if (!rend.childNodes.hasOwnProperty(i))
                        continue;
                    
                    if (typeof temp.childNodes[i] === 'undefined')
                        temp.appendChild(new Text());
                        
                    this.update(temp.childNodes[i], rend.childNodes[i]);
                }
            }
            else {
                let cnt = rend.childNodes.length;
                let nodesMoved = 0;

                for (let i = 0; i < temp.childNodes.length; i++) {
                    if (i < cnt) {
                        temp.childNodes[i]
                            .replaceWith(rend.childNodes[i - nodesMoved]);

                        nodesMoved++;
                    }
                    else {
                        temp.childNodes[i].remove();
                    }
                }
            }
        }
    }

    /**
     * Generates default event listener on the form inputs.
     * 
     * When an element's `tagName` is found in the switch-case, an event
     * listener with corresponding event is generated to store element's value
     * into Elem's props when the value changes. The event is generated only
     * for elements that carry a `name` attribute; those without it are ignored.
     * 
     * @private
     * @param {HTMLElement} node - an element to be checked and "eventualized"
     */
    invokeAutoEvents(node) {
        let event = null;

        if (typeof node.attributes.name === 'undefined')
            return;

        switch (node.tagName) {
            case 'INPUT':
                switch (node.attributes.type.value) {
                    case 'text':
                    case 'number':
                    case 'phone':
                        case 'email':
                        case 'range':
                        event = 'input';
                        break;
                        
                    case 'radio':
                    case 'checkbox':
                    default:
                        event = 'change';
                        break;
                }
                break;
            
            case 'TEXTAREA':
                event = 'input';
                break;
            
            case 'BUTTON':
                event = 'click';
                break;
            
            default:
                return;
        }

        node.addEventListener(event, (ev) => {
            ((ev, me) => {
                this.elem.setValue(ev.target.name, ev.target.value);
            })(ev, this.elem);
        });
    }

    /**
     * Sets event listeners to given element.
     * 
     * If an element carries a `data-e` attribute ("e" stands for "event"),
     * the content of the `dataset.e` is parsed. There can be multiple callbacks
     * defined, separated by space. Each callback is defined by listener event 
     * and function name in following format:
     * 
     * event:function
     * 
     * The listener event is default event of `addEventListener()` function and 
     * function name is a path to the Elem's props attribute.
     * 
     * Generated function has 2 arguments - 1st argument is default target event
     * and 2nd argument is Elem itself.
     * 
     * @private
     * @param {HTMLElement} element - DOM element to be enriched by events
     * @param {string} datasetE     - dataset.e to be parsed
     */
    findAndInvokeEvents(element, datasetE) {
        let matches = datasetE.matchAll(/([a-z\d_]+?):([a-z\d_]+)/gi);
        let match;

        do {
            match = matches.next();

            if (typeof match.value === 'undefined')
                continue;
            
            let event = this.elem.getValue(match.value[2]);

            element.addEventListener(
                match.value[1],
                (ev) => {event(ev, this.elem)},
                false
            );
        }
        while (!match.done);
    }

    /**
     * Returns DocumentFragment or Node with content generated by Elem's props.
     * 
     * If an Elem's template carries a {variable} in any attribute or in 
     * textContent itself, it is replaced by corresponding Elem's props value.
     * The variable is passed into Elem.getValue() method and based on its
     * instance it is transfered either into DocumentFragment or Node.
     *
     * @private 
     * @param {*} strNode     - node containing {variable}
     * @param {*} isAttribute - setter whether it is an attribute node or 
     *                          HTMLElement node
     */
    findAndReplaceBindings(strNode, isAttribute = false) {
        let match;
        let strNodeValue = isAttribute 
            ? strNode.valueOf().textContent
            : strNode.textContent;

        let matches = strNodeValue.matchAll(/\{(.+?)\}/g);
        let tempFragment = document.createDocumentFragment();

        do {
            match = matches.next();
            if (typeof match.value !== 'undefined') {
                let propsValue = this.elem.getValue(match.value[1]);

                let nv = this.nodizeValue(
                    propsValue,
                    strNodeValue,
                    match.value[0]
                );

                if (nv instanceof DocumentFragment) {
                    if (nv.childNodes.length) {
                        tempFragment.append(... nv.childNodes);
                    }
                    else {
                        tempFragment.appendChild(new Text());
                    }
                }
                else {
                    tempFragment.appendChild(nv);
                }
            }
        }
        while (!match.done);

        return (tempFragment.childNodes.length)
            ? tempFragment
            : strNode;
    }

    /**
     * Translates given value into a Node.
     * 
     * Takes any value and, based on its instance, translates it either into
     * HTMLElement or a Text Node.
     * 
     * @param {mixed} propsValue - value translated into Node
     * @param {*} strNodeText    - target string that is going to be enriched
     *                             by values from `propsValue` argument
     * @param {*} strToReplace   - part of string defined in `strNodeText` that
     *                             is going to be replaced
     */
    nodizeValue(propsValue, strNodeText, strToReplace) {
        if (propsValue instanceof Array) {
            let fragment = document.createDocumentFragment();
            propsValue.forEach((x) => {
                let vls = this.nodizeValue(x, strNodeText, strToReplace);

                if (vls instanceof NodeList) {
                    fragment.appendChild(... vls);
                }
                else {
                    fragment.appendChild(vls);
                }
            });
            return fragment;
        }
        
        if (propsValue instanceof Elem) {
            return propsValue.renderer.render();
        }
        
        if (typeof propsValue !== 'object') {
            return new Text(strNodeText.replace(strToReplace, propsValue));
        }

        return new Text(JSON.stringify(propsValue));
    }
}
